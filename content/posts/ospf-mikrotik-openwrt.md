---
title: "Динамическая маршрутизация OSPF между Mikrotik и Openwrt"
date: 2020-05-09T19:47:12+03:00
draft: true
toc: false
images:
tags:
  - mikrotik
  - openwrt
  - ospf
  - quagga
---
## Постановка задачи
Необходимо настроить протокол динамической маршрутизации OSPF. Конфигурация минимальна, с одной областью и не требует особых изяществ.

Между Mikrotik и OpenWRT в прошлой [статье](/posts/2020/05/gre-туннель-между-mikrotik-и-openwrt/) подняли туннель.

Имеем:
1. Сеть №1 (Mikrotik):
    1. WAN - 1.1.1.1
    2. IP туннеля - 172.16.0.1
    3. Netmask - /30 (255.255.255.252) 
    4. LAN - 192.168.0.0/24

2. Сеть №2 (OpenWRT):
    1. WAN - 2.2.2.2
    2. IP туннеля - 172.16.0.2
    3. Netmask - /30 (255.255.255.252) 
    4. LAN - 192.168.1.0/24

## Настройка Mikrotik
Для поставленной задачи конфиг минимален.
```
/routing ospf network> add network=172.16.0.0/30 area=backbone
/routing ospf network> add network=192.168.0.0/24 area=backbone
```

## Настройка OpenWRT

1. Необходимо установить Quagga
```
opkg update
opkg install quagga quagga-ospfd quagga-zebra
```

2. Запускаем демон 
``` 
/etc/init.d/quagga start
```

3. Подключаемся к ospfd
```
nc locahost ospfd
```

пароль по умолчанию *zebra*, посмотреть можно в файле: */etc/quagga/ospfd.conf*

Далее
```
OpenWRT> enable
OpenWRT# configure terminal
OpenWRT(config)# router ospf
OpenWRT(config-router)# network 172.16.0.0/30 area 0
OpenWRT(config-router)# network 192.168.1.0/24 area 0
OpenWRT(config-router)# exit
OpenWRT(config)# write
OpenWRT(config)# exit
OpenWRT# exit
```
4. Перезапускаем quagga
```
/etc/init.d/quagga restart
```
5. Ждем несколько мгновений и проверяем маршруты
```
ip route list
```

