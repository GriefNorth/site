---
title: "Установка Bareos на openSUSE Leap 15.2"
date: 2021-02-25T09:17:55+03:00
draft: true
tags:
  - openSUSE
  - Bareos
  - Backup
  - Linux
---

## Обновляем дистрибутив до актуального состояния

```bash
sudo zypper ref
sudo zypper up
```

<!--more-->

## Установка и запуск сервера БД

Так как в официальной документации Bareos MySQL и Sqlite имеют статус deprecated,
то устанавливаем PostgreSQL.

```bash
sudo zypper in postgresql postgresql-server postgresql-contrib
```

Активируем и запускаем сервер

```bash
sudo systemctl enable postgresql
sudo systemctl start postgresql
```

Установим пароль для пользователя _postgres_

```bash
sudo su
su - postgres
psql
postgres=# \password postgres
postgres=# \q
```

## Установка Bareos

Воспользуемся скриптом из документации и приведем его к следующему виду:

```bash
cat >> install_bareos.sh << 'EOF'
#!/bin/sh
DIST=openSUSE_Leap_15.2
RELEASE=release/20
URL=http://download.bareos.org/bareos/$RELEASE/$DIST
sudo zypper addrepo --refresh $URL/bareos.repo
sudo zypper install bareos bareos-database-postgresql
EOF
```

Запускаем установку `sh install_bareos.sh`, вводим пароль _sudo_, принимаем ключи
для репозитория, соглашаемся с установкой и ждем ее завершения.

## Подготовка БД для Bareos

```bash
sudo su
su - postgres
/usr/lib/bareos/scripts/create_bareos_database
/usr/lib/bareos/scripts/make_bareos_tables
/usr/lib/bareos/scripts/grant_bareos_privileges
```

## Активация и запуск сервисов

```bash
sudo systemctl enable --now bareos-dir
sudo systemctl enable --now bareos-sd
sudo systemctl enable --now bareos-fd
```

На этом все, не забываем что для демонов Bareos необходим доступ к портам 9101-9103

```bash
sudo firewall-cmd --permanent --add-port=80/tcp
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --permanent --add-port=9101/tcp
sudo firewall-cmd --permanent --add-port=9102/tcp
sudo firewall-cmd --permanent --add-port=9103/tcp
sudo firewall-cmd --reload
```
