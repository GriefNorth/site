---
title: "GRE-туннель между Mikrotik и Openwrt"
date: 2020-05-09T10:46:33+03:00
draft: true
toc: false
images:
tags:
  - mikrotik
  - openwrt
  - gre
  - tunnels
---
## Постановка задачи
Необходимо связать между собой две сети GRE-туннелем, шифрование не требуется.

Имеем:
1. Сеть №1 (Mikrotik):
    1. WAN - 1.1.1.1
    2. IP туннеля - 172.16.0.1
    3. Netmask - /30 (255.255.255.252) 

2. Сеть №2 (OpenWRT):
    1. WAN - 2.2.2.2
    2. IP туннеля - 172.16.0.2
    3. Netmask - /30 (255.255.255.252) 


## Настройка Mikrotik
1. Создаем GRE туннель
```
/interface gre add name=toOpenWRT remote-address=2.2.2.2
```

2. Назначаем адрес туннелю
```
/ip address
  add address=172.16.0.1/30 interface=toOpenWRT
```

3. Разрешаем GRE подключение
```
/ip firewall filter
  add action=accept chain=input protocol=gre comment="Allow-GRE"
```


## Настройка OpenWRT
WEB-интерфейс OpenWRT  не поддерживает настройку GRE-туннелей, поэтому все действия выполняем через консоль.

1. Установка необходимых пакетов
```
opkg update
opkg install kmod-gre kmod-nf-nathelper-extra gre ip-full
```

2. Конфигурирование сетевого интерфейса */etc/config/network*
```
config interface 'gre'
        option peeraddr '1.1.1.1'
        option proto 'gre'
        option zone 'gre'

config interface 'gre_static'
        option proto 'static'
        option ifname '@gre'
        option ipaddr '172.16.0.2'
        option netmask '255.255.255.252'
        option zone 'gre'
```

3. Сохраняем и перезагружаем конфиг сети
```
/etc/init.d/network reload
```

4. Настройка firewall-a */etc/config/firewall*

```
config rule
      option name 'Allow-GRE'
      option src 'wan'
      option proto '47'
      option family 'ipv4'
      option target 'ACCEPT'

config zone
        option name 'gre'
        option input 'REJECT'
        option output 'ACCEPT'
        option forward 'REJECT'

config forwarding
        option src 'lan'
        option dest 'gre'

config forwarding
        option src 'gre'
        option dest 'lan'        
```
5. Сохраняем и перезагружаем конфиг firewall-а
```
/etc/init.d/firewall reload
```

Таким образом связали две сети на маршрутизаторах Mikrotik и OpenWRT, посредством GRE-туннеля. Настройка маршрутизации между сетями в следующей [заметке](/posts/2020/05/динамическая-маршрутизация-ospf-между-mikrotik-и-openwrt/).