---
title: "Настройка DNS over TLS и DNSSEC на Openwrt"
date: 2020-05-22T22:54:32+03:00
draft: true
toc: false
images:
tags:
  - openwrt
  - stubby
  - dot
  - dnssec
---
Так как уже [установили](/posts/2020/05/openwrt-прозрачное-проксирование-в-сеть-tor/) **dnsmasq-full** перейдем непосредственно к настройке всего остального
## Установка stubby
Логинемся по ssh к OpenWRT и выполняем
```bash
opkg update
opkg install stubby
```
Далее включаем ручной режим в **/etc/config/stubby**
```conf
config stubby 'global'
       option manual '1'
``` 
Пример конфига, порт и серверы вольны менять как захотите.
```yaml
resolution_type: GETDNS_RESOLUTION_STUB
round_robin_upstreams: 1
appdata_dir: "/var/lib/stubby"
tls_authentication: GETDNS_AUTHENTICATION_REQUIRED
tls_query_padding_blocksize: 128
dnssec_return_status: GETDNS_EXTENSION_TRUE
edns_client_subnet_private: 1
idle_timeout: 10000
listen_addresses:
  - 127.0.0.1@5453
  - 0::1@5453
dns_transport_list:
  - GETDNS_TRANSPORT_TLS
upstream_recursive_servers:
  - address_data: 2606:4700:4700::1111
    tls_auth_name: "cloudflare-dns.com"
  - address_data: 2606:4700:4700::1001
    tls_auth_name: "cloudflare-dns.com"
  - address_data: 1.1.1.1
    tls_auth_name: "cloudflare-dns.com"
  - address_data: 1.0.0.1
    tls_auth_name: "cloudflare-dns.com"
```
Чтобы заработал DNSSEC добавил строку **dnssec_return_status: GETDNS_EXTENSION_TRUE**.

Активируем и перезапускаем:
```bash
/etc/init.d/stubby enable 
/etc/init.d/stubby start
```
## Настройка Dnamasq

В **/etc/dnsmasq.conf** меняем сервер на stubby
```conf
server=127.0.0.1#5453
```
Перезапускаем
```bash
/etc/init.d/dnsmasq restart
```
Идем проверять:
1. DNS over TLS -> [1.1.1.1/help](https://1.1.1.1/help)  
2. DNSSEC -> [https://dnssec.vs.uni-due.de/](https://dnssec.vs.uni-due.de/)

Так же в Firefox-е можно включить DNS over HTTPS Настройки -> Параметры сети -> Включить DNS через HTTPS