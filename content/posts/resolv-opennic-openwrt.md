---
title: "Resolve Opennic доменов на OpenWRT"
date: 2020-05-15T20:49:32+03:00
draft: true
toc: false
images:
tags:
  - openwrt
  - opennic
  - dnsmasq
---

## Настройка dnsmasq

Заходим на сайт [Opennic](https://www.opennic.org/) для получения списка ближайших серверов. В моем случае это:

```
91.217.137.37 (ns5.ru)
176.126.70.119 (ns1.sto.se)
192.71.245.208 (ns2.lom.it)
195.10.195.195 (ns31.de)
```
Так как в [прошлой](/posts/2020/05/openwrt-прозрачное-проксирование-в-сеть-tor/) статье установили и настроили dnsmasq для резолва .onion сайтов, то на OpenWRT просто 
редактируем **/etc/dnsmasq.conf** и добавляем следующие строки:
```
server=/bbs/chan/cyb/dyn/geek/gopher/indy/libre/neo/null/o/oss/oz/parody/pirate/lib/91.217.137.37
server=/bbs/chan/cyb/dyn/geek/gopher/indy/libre/neo/null/o/oss/oz/parody/pirate/lib/176.126.70.119
server=/bbs/chan/cyb/dyn/geek/gopher/indy/libre/neo/null/o/oss/oz/parody/pirate/lib/192.71.245.208
server=/bbs/chan/cyb/dyn/geek/gopher/indy/libre/neo/null/o/oss/oz/parody/pirate/lib/195.10.195.195
```
Где IP из указанного выше списка.

Перезапускаем dnsmasq
```
/etc/init.d/dnsmasq restart
```

Проверяем доступность того же [Рутрекера](http://rutracker.lib/forum/index.php) 