---
title: "Собственный репозиторий OpenWRT на Gitlab-e"
date: 2020-05-18T23:05:10+03:00
draft: true
toc: false
images:
tags:
  - openwrt
  - gitlab
  - docker
  - ci/cd
---
Данная статья поможет создать собственный репозиторий OpenWRT, со сборкой пакетов и хостингом их, средствами 
Gitlab Pages и CI/CD. 

## Cоздание пары ключей для подписи пакетов
1. Клонируйте репозиторий usign и скомпилируйте его. Обратите внимание, для успешной компиляции 
требуется установленный **cmake**. 
```bash
$ git clone https://git.openwrt.org/project/usign.git 
$ cd usign/ 
$ cmake . 
$ make
```

2. Даем команду исполняемому файлу **usign** создать новую пару ключей и вводим 
соответствующий комментарий, чтобы позже можно было идентифицировать файл ключа.
```
./usign -G -c "My own OpenWRT repo" -s secret.key -p public.key
```
Ключи готовы, к ним вернемся позже.

## Окружение для сборки
1. Подготавливаем папку с репозиторием хранилица пакетов OpenWRT
```bash
$ mkdir openwrt-repo
$ cd openwrt-repo
``` 
2. Создаем **Dockerfile** со следующим содержанием:
```Dockerfile
FROM debian:stable-slim
MAINTAINER name <https://hub.docker.com/u/name/>
RUN apt --quiet update --yes
RUN apt --quiet install --yes subversion g++ zlib1g-dev build-essential git python rsync man-db libncurses5-dev gawk gettext unzip file libssl-dev wget zip time
RUN wget -qO- https://downloads.openwrt.org/releases/19.07.2/targets/ar71xx/generic/openwrt-sdk-19.07.2-ar71xx-generic_gcc-7.5.0_musl.Linux-x86_64.tar.xz | tar xJf -
RUN mv openwrt* openwrtsdk
```
Где мейнтейнером указываем себя. Устанавливаем необходимые зависимости и в строке, со скачиванием openwrt-sdk, 
вставляем ссылку на нужную вам архитектуру. 
3. Билдим образ и тегируем его
```bash
$ docker build -t name/openwrt-repo .
```
4. Заливаем на [hub.docker.com](https://hub.docker.com/)
```bash
$ docker login
$ docker push name/openwrt-repo:latest
```
С докером покончено, ждем когда сбилдится на сайте и переходим к Gitlab.

## Настройка Gitlab

1. Создаем новый проект

![Создание проекта](/img/openwrt-repo/gitlab-cp.png)

2. Переходим в настройки проекта 
**Settings -> CI/CD**

И в Variables (переменных) указываем ранее созданные ключи 

![Настройки CI/CD](/img/openwrt-repo/gitlab-cicd-settings.png)

Где GPG_PRIVATE_KEY - secret.key, GPG_PUBLIC_KEY - public.key. Обратите внимание на символ перевода строки **\n**  после "untrusted comment: My own OpenWRT repo" 

3. Добавляем в проект файл **.gitlab-ci.yml** 
```yaml
image: name/openwrt-repo

pages:
  stage: deploy
  variables:
    GPG_PUBLIC_TEST: to_be_set
  script:
  - mkdir /openwrtpackages
  - echo -e "src-link name /openwrtpackages\n" >> /openwrtsdk/feeds.conf.default
  - git clone https://gitlab.com/Name/name-of-package.git /openwrtpackages/name-of-package
  - /openwrtsdk/scripts/feeds update name
  - /openwrtsdk/scripts/feeds install -a -p name
  - cd /openwrtsdk
  - make defconfig
  - make package/name-of-package/compile V=s
  - echo -e $GPG_PUBLIC_KEY >> key-build.pub
  - echo -e $GPG_PRIVATE_KEY >> key-build
  - make package/index V=s
  - mkdir "$CI_PROJECT_DIR/public"
  - cp key-build.pub "$CI_PROJECT_DIR/public"
  - cp -r /openwrtsdk/bin/packages/mips_24kc/name/* "$CI_PROJECT_DIR/public"
  artifacts:
    paths:
    - public
  only:
  - master

```
Где image - имя образа на dockerhub-е, name-of-package - имя вашего пакета, и его репозиторий, name - ваше имя пользователя. 

Ждем прохождения тестов и деплоя. 

4. Переходим в **Settings -> Pages** тут на ваше усмотрение, можете использовать предложенный адрес или добавить свой домен. 

## Добавление созданного репозитория
Логинимся в OpenWRT и выполняем следующее:

```bash
echo -e -n 'untrusted My own OpenWRT repo\nPUBLIC_KEY\n' > /tmp/my-own-repo.pub && opkg-key add /tmp/my-own-repo.pub

! grep -q 'my_own_repo' /etc/opkg/customfeeds.conf && echo 'src/gz my_own_repo http://link_from_gitlab_pages.com' >> /etc/opkg/customfeeds.conf
```
Не забудьте указать корректную ссылку на репозиторий, полученную из GitLab Pages. 

На этом все! 

