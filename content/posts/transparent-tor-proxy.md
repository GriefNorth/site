---
title: "OpenWRT прозрачное проксирование в сеть Tor"
date: 2020-05-15T18:20:31+03:00
draft: true
toc: false
images:
tags:
  - openwrt
  - tor
  - proxy
  - onion
---

## Установка пакетов
```
opkg update
opkg install ipset tor tor-geoip dnsmasq-full
```

## Настройка Tor-a 

Отрываем на редактирование **/etc/tor/torrc**
```
cp /etc/tor/torrc /etc/tor/torrc.opkg
vim /etc/tor/torrc
```
И приводим конфиг к такому виду
```
User tor
PidFile /var/run/tor.pid
DataDirectory /var/lib/tor
ExcludeExitNodes {RU},{UA},{AM},{KG}
VirtualAddrNetwork 10.8.0.0/16
AutomapHostsSuffixes .onion,.exit
AutomapHostsOnResolve 1

TransPort 192.168.1.1:9040
SOCKSPort 192.168.1.1:9050
DNSPort   9053
```

Перезапускаем Tor
```
/etc/init.d/tor restart
```

## Настройка firewall-a

Добавляем в **/etc/config/firewall**
```
config ipset
	option name 'onion'
	option storage 'hash'
	option match 'dest_ip'
	option timeout '86400'

config redirect
	option name 'torify-onion'
	option src 'lan'
	option proto 'tcp'
	option ipset 'onion'
	option dest_port '9040'
	option dest 'lan'
```

Перезагружаем firewall
```
/etc/init.d/firewall reload
```

## Настройка Dnsmasq

В файле **/etc/config/dhcp**, секцию с dnsmasq приводим к след виду:
```
config dnsmasq
	option domainneeded '1'
	option localise_queries '1'
	option local '/lan/'
	option domain 'lan'
	option expandhosts '1'
	option authoritative '1'
	option readethers '1'
	option leasefile '/tmp/dhcp.leases'
	option nonwildcard '1'
	option rebind_protection '0'
	option localservice '0'
	option noresolv '1'
```

Далее правим непосредственно **/etc/dnsmasq.conf**

```
server=1.1.1.1#53 
server=/onion/127.0.0.1#9053
ipset=/onion/onion
```

Перезапускаем dnsmasq
```
/etc/init.d/dnsmasq restart
```

Проверяем доступность .onion сайтов, для примера заходим на [DuckDuckGo](https://3g2upl4pq6kufc4m.onion)
